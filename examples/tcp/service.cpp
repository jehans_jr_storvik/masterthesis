// This file is a part of the IncludeOS unikernel - www.includeos.org
//
// Copyright 2015-2016 Oslo and Akershus University College of Applied Sciences
// and Alfred Bratterud
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
        An example to show incoming and outgoing TCP Connections.
        In this example, IncludeOS is listening on port 80.
	
        Data received on port 80 will be redirected to a
        outgoing connection to a (in this case) python server (server.py)
	
        Data received from the python server connection 
        will be redirected back to the client.

        To try it out, use netcat to connect to this IncludeOS instance.
 */

#include <os>
#include <net/inet4>
#include <apigateway.hpp>

//Defines a template
using Apigateway = http::Apigateway;

void Service::start() {
    Apigateway* gateway = new Apigateway();
}
