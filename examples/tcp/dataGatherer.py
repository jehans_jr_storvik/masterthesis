import subprocess
import time

print "This script will start the API Gateway service, and measure the total CPU used"
#process = subprocess.Popen('./testLinux', shell=False)
#print "\nTestLinux process started. PID = ", process.pid

#command = "/proc/" +  str(process.pid) + "/stat"
command = "/proc/6156/stat"
while True:
	proc = subprocess.Popen(["cat", command], stdout=subprocess.PIPE)
	output = proc.stdout.read().split()
	print "utime = ", output[15]
	print "stime = ", output[16]
	print "cutime = ", output[17]
	print "cstime = ", output[18]
	print "starttime =", output[23]
	time.sleep(10)
