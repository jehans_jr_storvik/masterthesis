#ifndef HTTP_APIGATEWAY_HPP
#define HTTP_APIGATEWAY_HPP
//#include <response.hpp>
//#include <message.hpp>
//#include <mime_types.hpp>
//#include <request.hpp>

using namespace std;

namespace http {
class Apigateway {
    private:
        string function;
public: 
    Apigateway();
    string getFunction(string request);
};

Apigateway::Apigateway() {
     /* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) {
        error("ERROR opening socket");
     }
     
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi("99");
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
              error("ERROR on binding");
     }
     listen(sockfd,5);
     
        clilen = sizeof(cli_addr);
        
     do {
         printf("Listening! Looping!\n");
         
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

        if (newsockfd < 0)  {
             error("ERROR on accept");
        }
        
        bzero(buffer,256);
        n = read(newsockfd,buffer,255);

        if (n < 0) error("ERROR reading from socket");

        printf("Here is the message: %s\n",buffer);
        
        n = write(newsockfd,"I got your message",18);

        if (n < 0) error("ERROR writing to socket");

        close(newsockfd);
        
     }while(true);
     close(sockfd);
     return 0;      
}

string Apigateway::getFunction(string request) {
    string function;
    for(int i = 4;;i++) {
      if (isspace(request.at(i))) {
        break;
      }
      function += request.at(i);
    }
    return function;
  }
void error(const char *msg)
{
    perror(msg);
    exit(1);
}
}
#endif //< APIGATEWAY_HPP
