#ifndef HTTP_API_HPP
#define HTTP_API_HPP

#include <vector>
#include <response.hpp>
#include <string>
#include <stdio.h>
#include <request.hpp>
#include <message.hpp>
#include <algorithm>
#include <mime_types.hpp>


using namespace std;
using Connection_ptr = shared_ptr<net::TCP::Connection>;

#define content_type http::header_fields::Entity::Content_Type
#define html         extension_to_type("html")

namespace http {
    class API {
        private:

    public:
        vector<string> allowed_methods; //POST - GET - PUT - DELETE        
        vector<net::TCP::Socket> server_list;//IP + Port. Enables loadbalancing. DNS will not be prioritised for this thesis, but is a feature creep.        

        string resource;          //Resource to fetch. Eg. /index.html, or /listOfCars
        Response res;
        string http;
        
        bool   cache_data;        //True or false. Default false.
        bool   cache_exists;
                
        //std::string cached_data;          //Contains cached data from API.
        //std::int cached_at;               //UNIX Epoch time when data last was cached
        //std::int TTL_cache;               //Number of seconds for cache to live. 
                                            //Feature creep is pre-emptive caching.
                                            //For now, only caching on outgoing request will be enabled.
        
        /*------------ Functions -----------*/
        API();
        API(string resource, net::TCP::Socket server, bool cache_data);
        
        void add_server(net::TCP::Socket server);
        net::TCP::Socket get_server();
        
        string getRequestHeader(string method);
        void connect_to_API(shared_ptr<net::Inet4<VirtioNet> > inet, Connection_ptr client, string method);
    };
/*------------------------------------------------------------------------------*/ 

    API::API(){};
    API::API(string resource, net::TCP::Socket server, bool cache_data) { 
        this->cache_data = cache_data;
        this->allowed_methods.push_back("GET");
        
        this->resource = resource;
        this->add_server(server);
    }
    
    void API::connect_to_API(shared_ptr<net::Inet4<VirtioNet> > inet, Connection_ptr client, string method) {
        //printf("Running function connect_to_api. \n");
        
        if (!(std::find(allowed_methods.begin(), allowed_methods.end(), method) != allowed_methods.end())) {
            Response res_error;
            res_error.add_header(content_type, html)
            .add_header(header_fields::Response::Connection, "close"s)
            .add_body("Method " + method + " not supported for this function.");
            this->http = res_error.to_string();
            client->write(http.data(), http.size());
            client->close();
            return;
        }
                
        if(cache_data == true && cache_exists == true) {
            //printf("Returning cached data\n");
            client->write(http.data(), http.size());
            client->close();
            return;
        }
        
        inet->tcp().connect(this->get_server())  
                
 		->onConnect([this, client, method](shared_ptr<net::TCP::Connection> API_server) {
                    
                    string reqHeader = this->getRequestHeader(method);
                        API_server->write(reqHeader.data(), reqHeader.size());
                        //When the connection on the IncludeOS instance receives data from the API 
                        API_server->onReceive([this, client](std::shared_ptr<net::TCP::Connection> API_server, bool) {
                            cache_exists = true;  
                            Request req;
                            string buffer;
                            API_server->read(buffer, 1024);
                            API_server->close();
                            
                            res.reset();                     
                            res.add_header(content_type, html)
                               .add_header(header_fields::Response::Connection, "close")
			       .add_body(req.get_body());
                                
                            //Do this on server disconnect
                            this->http = res_error.to_string();
                            client->write(http.data(), http.size());
                            client->close();
 			}); 
                        
                });
    }
    
    void API::add_server(net::TCP::Socket server) {
        this->server_list.push_back(server);
    }
    
    net::TCP::Socket API::get_server() {
        //Hent en av serverene, og IKKE den som sist ble kallt.        
        return this->server_list.at(rand() % this->server_list.size());
    }
    
    string API::getRequestHeader(string method) {
    return method + " " + this->resource + " HTTP/1.1 \n" \
                                      "Host: IncludeOS \n" \
                                      "Connection: close\r\n\r\n";
    }
    
}
#endif //API_HPP
