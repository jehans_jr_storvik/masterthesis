#ifndef HTTP_APIGATEWAY_HPP
#define HTTP_APIGATEWAY_HPP
#include <api.hpp>
#include <response.hpp>
#include <message.hpp>
#include <mime_types.hpp>
#include <request.hpp>


/*
 1 - Sjekke for valid HTTP metode
 2 - Loadbalancing
 3 - GUI
 4 - Metode som skriver til klient med korrekte HTTP headers. 
 
 */
using namespace std;
using Connection_ptr = shared_ptr<net::TCP::Connection>;
shared_ptr<net::Inet4<VirtioNet> > inet;

namespace http {
class Apigateway {
    private:
        map<string, API> API_list; //Contains a list of <function> --> <API> mapping. Eg. /cars -> carAPI
        string function;
public: 
    Apigateway();
    void init_API_list();
};

Apigateway::Apigateway() {
    hw::Nic<VirtioNet>& eth0 = hw::Dev::eth<0, VirtioNet>();
    inet = make_shared<net::Inet4<VirtioNet> >(eth0); 
    inet->network_config( {{ 10,0,0,42 }},    // IP
			{{ 255,255,255,0 }},  // Netmask
			{{ 10,0,0,1 }},       // Gateway
			{{ 8,8,8,8 }} );      // DNS
    auto& server = inet->tcp().bind(80);
    
    inet->dhclient()->on_config([&server](auto&) {
        printf("IncludeOS-Server IP updated: %s\n", server.local().to_string().c_str());
    });
        
    init_API_list();
    server.onReceive([this](Connection_ptr client, bool) {
            Request req;
            client->read(req, 1024);
            this->function = req.uri();
            
            if (!(API_list.find(this->function.c_str()) == API_list.end())) {
                this->API_list[this->function].connect_to_API(inet, client, req.method());
            }
             
            //If no match until now, return the admin page.
            else(this->function.compare("/functions") == 0) 
            {
                Response res;
                string message = "<h1>API Gateway - Functions</h1></br>";
                for(auto const &ent1 : API_list) {
                    message.append("<h2>Function - " + ent1.first + "</h2>");                    
                    message.append("Resource - " + ent1.second.resource);
                    message.append("</br>");
                    
                    message.append("Allowed_methods - ");
                    for(auto const &method : ent1.second.allowed_methods)  {
                        message.append(method + " - ");
                    }
                    message.append("</br>");
                    
                    string caching_enabled = (ent1.second.cache_data) ? "Yes" : "No";
                    message.append("Caching enabled - " + caching_enabled);
                    message.append("</br>");
                    
                    message.append("List of servers - </br>");
                    for(auto const &server : ent1.second.server_list)  {
                        message.append("Server -" + server.to_string() + "</br>");
                    }
                    message.append("</br>");
                    message.append("-------------------------------------------------------------------------------</br>");
                  }
                res.add_header(content_type, html)
                .add_header(header_fields::Response::Connection, "close"s)
                .add_body(message);
                client->write(res);
                client->close();
                return;
            }
            
            else {
                Response res;
                res.set_status_code(404);
                string message = "Site not found";
                res.add_header(content_type, html)
                .add_header(header_fields::Response::Connection, "close"s)
                .add_body(message);
                client->write(res);
                client->close();
                return;
            }
            
                });
}

void Apigateway::init_API_list() {
    net::TCP::Socket server_virtualgarage = net::TCP::Socket{{{128, 199, 46, 102}}, 80};    //Internet, VirtualGarage
    net::TCP::Socket server_carapi = net::TCP::Socket{{{10, 1, 6, 189}}, 80};               //Same subnet. carAPI
    net::TCP::Socket server_carapi2 = net::TCP::Socket{{{10, 1, 7, 92}}, 80};               //Same subnet. carAPI
    
    API_list.insert(map<string, API>::value_type("/virtualgarage", API("/getMostViewedBlogs", server_virtualgarage, true)));
    
    API_list.insert(map<string, API>::value_type("/carAPI", API("/", server_carapi, false))); 
    API_list["/carAPI"].add_server(server_carapi2);
}


}
#endif //< APIGATEWAY_HPP
