#ifndef HTTP_LINUXAPIGATEWAY_HPP
#define HTTP_LINUXAPIGATEWAY_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include <map>
#include <linuxapi.hpp>

void error(const char *msg);

namespace http {
class LinuxApigateway {
    private:
        map<string, LINUXAPI> API_list; //Contains a list of <function> --> <API> mapping. Eg. /cars -> carAPI
        std::string function;
public: 
    LinuxApigateway();
    void  init_API_list();
    std::string getFunction(std::string request);
};

LinuxApigateway::LinuxApigateway() {
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) {
        error("ERROR opening socket");
     }
     
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi("80"); //Incoming port to listen on.
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
              error("ERROR on binding");
     }
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
        
    init_API_list();
     do {         
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0)  { error("ERROR on accept"); }
        
        n = read(newsockfd,buffer,255);
        if (n < 0) { error("ERROR reading from socket"); }
        
        this->function = this->getFunction(buffer);        
        if (!(API_list.find(this->function.c_str()) == API_list.end())) {
            this->API_list[this->function].connect_to_API(newsockfd);
        } 
        else if(this->function.compare("/functions") == 0) 
            {
                string message = "<h1>API Gateway - Functions</h1></br>";
                for(auto const &ent1 : API_list) {
                    message.append("<h2>Function - " + ent1.first + "</h2>");                    
                    message.append("Resource - " + ent1.second.resource);
                    message.append("</br>");
                    
                    message.append("Allowed_methods - ");
                    for(auto const &method : ent1.second.allowed_methods)  {
                        message.append(method + " - ");
                    }
                    message.append("</br>");
                    
                    string caching_enabled = (ent1.second.cache_data) ? "Yes" : "No";
                    message.append("Caching enabled - " + caching_enabled);
                    message.append("</br>");
                    
                    message.append("List of servers - </br>");
                    for(auto const &server : ent1.second.server_list)  {
                        message.append("Server -" + server + "</br>");
                    }
                    message.append("</br>");
                    message.append("-------------------------------------------------------------------------------</br>");
                  }
            string header =  "HTTP/1.1 200 \n" \
                             "Content-Type: text/plain \n";
            header.append("Content-Length: " + to_string(message.size()) + " \n");
            header.append("Connection: close \r\n\r\n");
            header.append(message);
            n = write(newsockfd, header.c_str(), header.length());
            if (n < 0) { printf("ERROR writing to socket\n"); }
            close(newsockfd);
            }
        else 
        {
            string header =  "HTTP/1.1 404 Not Found \n" \
                             "Content-Type: text/plain \n";
            header.append("Content-Length: 18 \n");
            header.append("Connection: close \r\n\r\n");
            header.append("Function not found");
            n = write(newsockfd, header.c_str(), header.length());
            if (n < 0) { printf("ERROR writing to socket\n"); }
            close(newsockfd);
        }        
     }while(true);
     close(sockfd);
}

void LinuxApigateway::init_API_list() {
    API_list.insert(map<string, LINUXAPI>::value_type("/api1", LINUXAPI("/index.html", false, "128.39.121.118")));
    this->API_list["/api1"].add_server("128.39.121.114");
    
    API_list.insert(map<string, LINUXAPI>::value_type("/api2", LINUXAPI("/getMostViewedBlogs", true, "128.39.121.118")));

}

std::string LinuxApigateway::getFunction(std::string request) {
    std::string function;
    for(int i = 4;;i++) {
      if (isspace(request.at(i))) {
        break;
      }
      function += request.at(i);
    }
    return function;
  }

}

void error(const char *msg)
    {
        perror(msg);
        exit(1);
    }
#endif //< HTTP_APIGATEWAY_HPP

